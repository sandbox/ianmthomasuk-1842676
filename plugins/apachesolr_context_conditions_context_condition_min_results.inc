<?php

/**
 * Expose number of Apache Solr results as a context condition.
 */
class apachesolr_context_conditions_context_condition_min_results extends context_condition {

  /**
   * Omit condition values. We will provide a custom input form for our conditions.
   */
  function condition_values() {
    return array();
  }

  /**
   * Condition form.
   */
  function condition_form($context) {
    $form = parent::condition_form($context);
    unset($form['#options']);

    $form['#type'] = 'textfield';
    $form['#default_value'] = implode("\n", $this->fetch_from_context($context, 'values'));
    $form['#description'] = 'Set this context when there are at least the above number of results returned by the last Apachesolr search. Particularly useful for blocks that should not show on a no results page (by setting it to 1).';
    return $form;
  }

  /**
   * Condition form submit handler.
   */
  function condition_form_submit($values) {
    $parsed = array();
    $items = explode("\n", $values);
    if (count($items)) {
      foreach ($items as $v) {
        $parsed[$v] = trim($v);
      }
    }
    return $parsed;
  }

  /**
   * Execute.
   */
  function execute($num_results = 0) {
    if ($this->condition_used()) {
      foreach ($this->get_contexts() as $context) {
        $required_min = array_pop($this->fetch_from_context($context, 'values'));
        if ($num_results >= $required_min) {
          $this->condition_met($context);
        }
      }
    }
  }
}
